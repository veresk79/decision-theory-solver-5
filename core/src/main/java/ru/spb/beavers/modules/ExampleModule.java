package ru.spb.beavers.modules;

import org.scilab.forge.jlatexmath.TeXConstants;
import org.scilab.forge.jlatexmath.TeXFormula;
import org.scilab.forge.jlatexmath.TeXIcon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class ExampleModule implements ITaskModule {

	private class SaveListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.out.println("SaveListener performed");
		}
	}

	private class LoadListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.out.println("LoadListener performed");
		}
	}

	private class DefaultValuesListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.out.println("DefaultValuesListener performed");
		}
	}

	private SaveListener saveListener = null;
	private LoadListener loadListener = null;
	private DefaultValuesListener defaultValuesListener = null;

	/**
	 * @return Название задачи. 1 - 4 слова.
	 */
	public String getTitle() {
		return "2.1.2. Задача о газетах";
	}

	/**
	 * Инициализация элементов на панели, дающей краткое описание решаемой задачи.
	 *
	 * @param panel Инициализируемая панель.
	 */
	public void initDescriptionPanel(JPanel panel) {
		descriptionTextArea.setBounds(0, 0, 600, 900);
		descriptionTextArea.setLineWrap(true);
		descriptionTextArea.setWrapStyleWord(true);
		descriptionTextArea.setText("Неформальная постановка задачи\n" +
				"\n" +
				"Продавец газет ежедневно закупает газеты по оптовой цене, а затем продает их по розничной. Не проданные в течение дня газеты полностью теряют свою стоимость. Спрос на газеты определяется различными внешними факторами и не носит регулярного характера. Доход продавца определяется количеством проданных газет и разницей между оптовой и розничной стоимостью газет. Если продавец закупает недостаточно газет и спрос на газеты превышает имеющееся количество газет то продавец не получит части дохода, которую мог бы получить, закупив большее число газет. Если продавец закупает слишком много газет и часть газет остается не проданными, то продавец теряет на каждой непроданной газете ее оптовую стоимость. Задача сводится к выбору количества газет, закупка и продажа которых принесут продавцу наибольший доход.");
//		panel.add(descriptionTextArea);

//		Я нашел библиотеку JLaTeXMath чтобы формулы строить:
//		http://forge.scilab.org/index.php/p/jlatexmath/
//		Тут пример:
//		http://www.heatonresearch.com/node/2868

		try {
			// get the text
			String latex = "x=\\frac{-b \\pm \\sqrt {b^2-4ac}}{2a}";

			// create a formula
			TeXFormula formula = new TeXFormula(latex);

			// render the formla to an icon of the same size as the formula.
			TeXIcon icon = formula.createTeXIcon(TeXConstants.STYLE_DISPLAY, 20);

			// insert a border
			icon.setInsets(new Insets(5, 5, 5, 5));

			// now create an actual image of the rendered equation
			BufferedImage image = new BufferedImage(icon.getIconWidth(),
					icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);

			Graphics2D g2 = image.createGraphics();
			g2.setColor(Color.white);
			g2.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());

			JLabel jl = new JLabel();
			jl.setForeground(new Color(0, 0, 0));
			icon.paintIcon(jl, g2, 0, 0);
			// at this point the image is created, you could also save it with ImageIO

			JLabel formulaIcon = new JLabel(new ImageIcon(image));
			formulaIcon.setBounds(20, 20, 200, 100);

			panel.add(formulaIcon);

		} catch (Exception ex) {
			ex.printStackTrace();
		}


		panel.setPreferredSize(new Dimension(850, 900));



	}

	private JTextArea descriptionTextArea = new JTextArea(20, 70);

	/**
	 * Инициализация элементов на панели, предоставляющие подробное решение задачи.
	 * Выводится полная теоритическая справка, вывод всех формул, графиков, описание входных/выходных данных задачи.
	 *
	 * @param panel Инициализируемая панель.
	 */
	public void initSolutionPanel(JPanel panel) {
		solutionTextArea.setLineWrap(true);
		solutionTextArea.setWrapStyleWord(true);
		solutionTextArea.setText("Подробное описание задачи.");
		panel.add(solutionTextArea);
	}

	private JTextArea solutionTextArea = new JTextArea(20, 70);

	/**
	 * Инициализация элементов панели ввода исходных данных.
	 *
	 * @param panel Инициализируемая панель.
	 */
	public void initInputPanel(JPanel panel) {
		pelmenLabel.setText("Стоимость запупки: ");
		pelmenLabel.setEnabled(false);
		pelmenLabel.setEditable(false);
		pelmen.setEditable(true);

		kartoshkaLabel.setText("Стоимость продажи: ");
		kartoshkaLabel.setEnabled(false);
		kartoshkaLabel.setEditable(false);
		kartoshka.setEditable(true);

		baklazhanLabel.setText("Количество газет: ");
		baklazhanLabel.setEnabled(false);
		baklazhanLabel.setEditable(false);
		baklazhan.setEditable(true);

		soupLabel.setText("Суп с котом: ");
		soupLabel.setEnabled(false);
		soupLabel.setEditable(false);
		soup.setEditable(true);

		panel.add(pelmenLabel);
		panel.add(pelmen);
		panel.add(kartoshkaLabel);
		panel.add(kartoshka);
		panel.add(baklazhanLabel);
		panel.add(baklazhan);
		panel.add(soupLabel);
		panel.add(soup);
	}

	private JTextField pelmenLabel = new JTextField();
	private JTextField pelmen = new JTextField(12);
	private JTextField kartoshkaLabel = new JTextField();
	private JTextField kartoshka = new JTextField(12);
	private JTextField baklazhanLabel = new JTextField();
	private JTextField baklazhan = new JTextField(12);
	private JTextField soupLabel = new JTextField();
	private JTextField soup = new JTextField(30);

	/**
	 * Инициализация элементов панели оторбражающей решение задачи с введенными исходными данными.
	 *
	 * @param panel Инициализируемая панель.
	 */
	public void initExamplePanel(JPanel panel) {
		panel.add(outputTextArea);
		outputTextArea.setText("");

		//Вход
		double Vc; //Стоимость закупки
		double Vs; //Стоимость продажи
		int m; //Количество газет для покупки
		double[] p; //Массив вероятностей

		//Выход
		int d = 0; //Оптимальное количество газет
		double[] U; //Массив полезностей

		//Алгоритм решения
		//Введем входные параметры
		Vc = Integer.parseInt(pelmen.getText());
		Vs = Integer.parseInt(kartoshka.getText());
		m = Integer.parseInt(baklazhan.getText());
		//*********************************
		//Фокусы разработчиков
		//Для соответсвия количества газет (указали 5 газет, а получаем 4)
		m++;
		//*********************************
		p = new double[m];


		String[] items = soup.getText().split(",");
		for (int i = 0; i < items.length; i++) {
			try {
				p[i] = Double.parseDouble(items[i]);
			} catch (NumberFormatException nfe) {
				System.out.println(nfe.toString());
			}
		}

		//Проверка значений вероятности
		double proverkaP = 0;
		for (int i = 0; i < m; i++) {
			proverkaP += p[i];
		}
		if (proverkaP == 1) {
			outputTextArea.append("Ты молодец!\n");
		}
		else {
			outputTextArea.append("Ты НУБ!\n");
		}

		//Расчет полезностей
		U = new double[m];
		for (int i = 0; i < m; i++) {
			double Z = 0;
			for (int j = 0; j < i; j++) {
				Z += (i - j) * p[j];
			}
			U[i] = i * (Vs - Vc) - Vs * Z;
		}

		//Поиск максимального значения и сохранения индекса.
		double max = 0; //максимальное значение
		for (int i = 0; i < m; i++) {
			if (max < U[i]) {
				max = U[i];
				d = i;
			}
		}

		//Расчет ценностей.
		double[][] V;
		V = new double[m][m];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < m; j++) {
				if (j < i) {
					V[j][i] = Vs * j - Vc * i;
				}
				else {
					V[j][i] = Vs * i - Vc * i;
				}
			}
		}

		//Вывод на экран полученных значений.
		outputTextArea.append("Расчет полезностей\n");
		System.out.println("");
		for (int i = 0; i < m; i++) {
			outputTextArea.append(U[i] + " \n\n");
		}
		// 0.1,0.2,0.3,0.4

		outputTextArea.append("Maximum " + max + "\n");
		outputTextArea.append("Index " + d + "\n");

		outputTextArea.append("Расчет ценностей\n");
		for (int i = 0; i < m; i++) {
			outputTextArea.append(i + " столбец\n");
			for (int j = 0; j < m; j++) {
				outputTextArea.append(V[j][i] + "\n");
			}
		}
	}

	private JTextArea outputTextArea = new JTextArea(20, 70);

	/**
	 * Listener может бросать IllegalArgumentException с описанием ошибки.
	 *
	 * @return Слушатель, который будет вызван для сохранения набора исходных данных в файл.
	 */
	public ActionListener getPressSaveListener() throws IllegalArgumentException {
		if (saveListener == null) {
			saveListener = new SaveListener();
		}
		return saveListener;
	}

	/**
	 * Listener может бросать IllegalArgumentException с описанием ошибки
	 *
	 * @return Слушатель, который будет вызван для загрузки набора исходных данных из файла.
	 */
	public ActionListener getPressLoadListener() throws IllegalArgumentException {
		if (loadListener == null) {
			loadListener = new LoadListener();
		}
		return loadListener;
	}

	/**
	 * Listener может бросать IllegalArgumentException с описанием ошибки
	 *
	 * @return Слушатель, который будет вызван для установки значений по умолчанию входных параметров задачи.
	 */
	public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
		if (defaultValuesListener == null) {
			defaultValuesListener = new DefaultValuesListener();
		}
		return defaultValuesListener;
	}
}

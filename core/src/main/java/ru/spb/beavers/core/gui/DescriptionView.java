package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.components.FixedJPanel;
import ru.spb.beavers.core.data.StorageModules;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View краткого описания модуля
 */
public class DescriptionView extends JPanel {

    private final JPanel descriptionPanel = new FixedJPanel();
    private final JButton btnGoMenu = new JButton("Меню");
    private final JButton btnGoTheory = new JButton("Теория");

    private final DescriptionViewPresenter presenter = new DescriptionViewPresenter();

    public DescriptionView() {
        super(null);

        descriptionPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        descriptionPanel.setSize(850, 400);
        descriptionPanel.setLayout( null );

        JScrollPane descriptionPanelWrapper = new JScrollPane(descriptionPanel);
        descriptionPanelWrapper.setSize(850, 400);
        descriptionPanelWrapper.setLocation(20, 20);
        this.add(descriptionPanelWrapper);

        btnGoMenu.setSize(100, 30);
        btnGoMenu.setLocation(20, 450);
        this.add(btnGoMenu);

        btnGoTheory.setSize(100, 30);
        btnGoTheory.setLocation(770, 450);
        this.add(btnGoTheory);

        initListeners();
    }

    private void initListeners() {
        btnGoMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoMenuPressed();
            }
        });

        btnGoTheory.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoTheoryPressed();
            }
        });
    }

    public JPanel getDescriptionPanel() {
        return descriptionPanel;
    }

    @Override
    public void removeAll() {
        descriptionPanel.removeAll();
    }

    private class DescriptionViewPresenter {

        public void btnGoMenuPressed() {
            GUIManager.setActiveView(GUIManager.getMenuView());
        }

        public void btnGoTheoryPressed() {
            TheoryView theoryView = GUIManager.getTheoryView();
            StorageModules.getActiveModule().initSolutionPanel(theoryView.getTheoryPanel());
            GUIManager.setActiveView(theoryView);
        }
    }
}
